package DAO;

import java.util.List;
import java.util.TreeMap;

import Model.Student;

public interface Student_DAO {

	public boolean saveStudent(Student student);
	public TreeMap<Integer,Student> getStudents();
	public boolean deleteStudent(Student student);
	public List<Student> getStudentByID(Student student);
	
	public boolean updateStudent(Student student);
}
